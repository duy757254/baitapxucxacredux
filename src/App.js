import logo from "./logo.svg";
import "./App.css";
import MainDiceRedux from "./diceReudx/MainDiceRedux";

function App() {
  return (
    <div className="App">
      <MainDiceRedux />
    </div>
  );
}

export default App;
