import React, { Component } from "react";
import bgGame from "./asset/bgGame.png";
import MainDice from "./MainDice";
import ResultDice from "./ResultDice";
import "./DiceStyle.css";

export default class MainRedux extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bgGame})`,
          width: "100vw",
          height: "100vh",
        }}
        className="main_bg"
      >
        <MainDice />
        <ResultDice />
      </div>
    );
  }
}
