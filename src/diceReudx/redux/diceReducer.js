let intialState = {
  diceArr: [
    {
      img: "./imgDice/1.png",
      value: 1,
    },
    {
      img: "./imgDice/1.png",
      value: 1,
    },
    {
      img: "./imgDice/1.png",
      value: 1,
    },
  ],
  choose: null,
  totalPlays: 0,
  totalPlayWins: 0,
};

export const diceReducer = (state = intialState, action) => {
  switch (action.type) {
    case "PLAY_GAME": {
      // duyệt mãng random
      let newDiceArr = state.diceArr.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgDice/${random}.png`,
          value: random,
        };
      });
      // mỗi lần click là được đếm số lần chơi
      state.totalPlays += 1;
      // tính tổng số xúc xắc được lắc
      let totalDice = newDiceArr.reduce((value, currentValue) => {
        return value + currentValue.value;
      }, 0);
      if (totalDice >= 11 && state.choose === "TÀI") {
        state.totalPlayWins += 1;
      }
      if (totalDice < 11 && state.choose === "XỈU") {
        state.totalPlayWins += 1;
      }

      return { ...state, diceArr: newDiceArr };
    }
    case "choice": {
      return { ...state, choose: action.payload };
    }
    default:
      return state;
  }
};
