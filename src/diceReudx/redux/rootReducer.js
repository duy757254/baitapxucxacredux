import { combineReducers } from "redux";
import { diceReducer } from "./diceReducer";

export const rootReducerDice = combineReducers({ diceReducer });
