import React, { Component } from "react";
import { connect } from "react-redux";

class MainDice extends Component {
  renderImage = () => {
    return this.props.diceObj.map((item, index) => {
      return (
        <img key={index} src={item.img} style={{ margin: 10, width: 120 }} />
      );
    });
  };
  render() {
    return (
      <div className="p-5">
        <h1 className="pb-5">Cùng Chơi Xúc Xắc Nào!!!</h1>
        <div className="dice_body d-flex container justify-content-between">
          <button
            onClick={() => {
              this.props.handleChoice("TÀI");
            }}
            style={{
              fontSize: 50,
              color: "black",
            }}
            className="rounded btn btn-success p-5 col-2"
          >
            Tài
          </button>
          <div className="">{this.renderImage()}</div>
          <button
            onClick={() => {
              this.props.handleChoice("XỈU");
            }}
            style={{
              fontSize: 50,
              color: "red",
            }}
            className="rounded btn btn-warning col-2 p-5"
          >
            Xỉu
          </button>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  diceObj: state.diceReducer.diceArr,
});

let mapDisPatchToProps = (dispatch) => {
  return {
    handleChoice: (dice) => {
      let action = {
        type: "choice",
        payload: dice,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDisPatchToProps)(MainDice);
