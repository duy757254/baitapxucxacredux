import React, { Component } from "react";
import { connect } from "react-redux";

class ResultDice extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.props.handlePlayGame();
          }}
          style={{
            width: 200,
            height: 80,
            fontSize: 20,
          }}
          className="btn btn-danger "
        >
          PLAY GAME
        </button>
        <div className="body_text ">
          <h2>Bạn đang chọn: {this.props.choice}</h2>
          <h2>Tổng số lần chơi là:{this.props.plays} </h2>
          <h2>Tổng số lần bạn thắng:{this.props.wins} </h2>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => ({
  choice: state.diceReducer.choose,
  plays: state.diceReducer.totalPlays,
  wins: state.diceReducer.totalPlayWins,
});
let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      let action = {
        type: "PLAY_GAME",
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultDice);
